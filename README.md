# 討論紀錄
-----
### 10/6

1. 切folds - 5 folds or 10(每個類別越平均越好) 
可參考: 

2. 對每一個維度作normalize(-1到1)

3. Learning rate: 0.05, 0.01, 0.005, 0.001, 0.0001

4. 策略: 先用minibatch找到一組不錯的組合,再用SGD(每個batch裡面最好有每個class的data)

5. 參考網址
 - http://scikit-learn.org/stable/modules/generated/sklearn.cross_validation.StratifiedKFold.html#sklearn.cross_validation.StratifiedKFold
 - http://deeplearning.net/tutorial/mlp.html

# 使用方法
-----
## merge.py
將.ark檔與.lab檔合併成一個檔案
    
    python merge.py 訓練資料檔 label檔 輸出檔名

## utils.py
### loadData()

讀取合併的檔案, X是feature vector, Y是label, n2lab是label-id與label-string的mapping

    X, Y, n2lab, frameID = utils.loadData(mergedFile)
    
### example
    In commad line: python merge.py mfcc/train.ark label/train.lab mfcc.merged
    In python shell: import utils; X,Y,n2lab,frameID = utils.loadData('path/to/mfcc.merged')