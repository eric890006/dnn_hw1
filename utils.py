import numpy as np

def loadData(mergedFile):
    """
    Load merged data (Feature vectors, LableIDs, LableID-to-Lable mapping and FrameIDs).
    Usage: IDs, Lables, Lable2n, vector = loadData(mergedFile)
    """
    with open(mergedFile) as f:
        lines = f.readlines()
    lables = []
    lable2n = {}
    ids = []
    vecs = []
    for line in lines:
        split = line.split()
        if split[0] not in lable2n:
            lable2n[int(split[0])] = split[1]
        lables.append(int(split[0]))
        ids.append(split[2])
        vec = []
        for i in range(3,len(split)):
            vec.append(float(split[i]))
        vec = np.array(vec, dtype='float32')
        vecs.append(vec)
    lables = np.array(lables, dtype='float32')
    vecs = np.array(vecs)
    ids = np.array(ids)
    return [vecs, lables, lable2n, ids]

