import sys

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'Usage: python merge.py trainFile lableFile outputFile'
        exit(0)
    
    trainFile = sys.argv[1]
    lableFile = sys.argv[2]
    outputFile = sys.argv[3]
    
    f1 = open(trainFile)
    f2 = open(lableFile)
    train = f1.readlines()
    lable = f2.readlines()
    f1.close()
    f2.close()

    id2lab = {}
    lab2n = {}
    cnt = 0
    for line in lable:
        ID = line.split(',')[0]
        LABLE = line.split(',')[1].strip()
        id2lab[ID] = LABLE
        if LABLE not in lab2n:
            lab2n[LABLE] = cnt
            cnt += 1

    l = []
    for line in train:
        ID = line.split()[0]
        LABLE = id2lab[ID]
        l.append(str(lab2n[LABLE]) + ' ' + LABLE + ' ' + line)
 
    f3 = open(outputFile,'w')       
    f3.writelines(l)
    f3.close()
